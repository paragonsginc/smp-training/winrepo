httpd:
  {% for version in ['2.2.22', '2.2.25'] %}
 '{{ version }}':
   full_name: 'Apache HTTP Server {{ version }}'
   installer: 'http://archive.apache.org/dist/httpd/binaries/win32/httpd-{{ version }}-win32-x86-no_ssl.msi'
   install_flags: '/qn /norestart ALLUSERS=1 SERVERADMIN=admin@localhost SERVERNAME=localhost SERVERDOMAIN=localhost SERVERPORT=80'
   uninstaller: 'http://archive.apache.org/dist/httpd/binaries/win32/httpd-{{ version }}-win32-x86-no_ssl.msi'
   uninstall_flags: '/qn /norestart'
   msiexec: True
   locale: en_US
   reboot: False
 {% endfor %}